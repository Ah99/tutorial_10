#pragma once

#include <vector>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"

namespace GC
{
	const float asteroid_speed = 100;
	const float asteroid_spawn_rate = 1;
	const float asteroid_spawn_inc = 1;
	const float asteroid_max_spawn_delay = 10;
	const int roid_cache = 32;
}


struct Bullet
{
	Bullet(MyD3D& d3d)
		:bullet(d3d)
	{}
	Sprite bullet;
	bool active = false;

	void Init(MyD3D& d3d);
	void Render(DirectX::SpriteBatch& batch);
	void Update(float dTime);
	const float MISSILE_SPEED = 300;
};

/*
Animated asteroid
*/
struct Asteroid
{
	Asteroid(MyD3D& d3d)
		:spr(d3d)
	{}
	Sprite spr;
	bool active = false;	//should it render and animate?

	//setup
	void Init();
	/*
	draw - the atlas has two asteroids, half frames in one, half the other
	asteroids are randomly assigned one and then animate and at random fps
	*/
	void Render(DirectX::SpriteBatch& batch);
	/*
	move left until offscreen, then go inactive
	*/
	void Update(float dTime);
};


//horizontal scrolling with player controlled ship
class PlayMode
{
public:
	PlayMode(MyD3D& d3d);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch);
	void Release();

	void introMode();
	void introRender(DirectX::SpriteBatch& batch);

	void gameoverMode();
	void gameoverRender(DirectX::SpriteBatch& batch);
	bool hasDied = false;
private:
	const float SCROLL_SPEED = 10.f;
	static const int BGND_LAYERS = 8;
	const float SPEED = 250;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
	int score = 0;

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers
	RECTF mPlayArea;	//don't go outside this	
	Sprite mPlayer;		//jet
	Sprite mThrust;		//flames out the back
	Bullet mMissile;	//weapon, only one at once

	//Asteroid Stuff
	std::vector<Asteroid> mAsteroids;
	float SpawnRateSec = GC::asteroid_spawn_rate;
	float lastSpawn = 0;
	Asteroid* spawnAsteroid();
	void updateAsteroids(float dTime);
	void initAsteroids();
	void renderAsteroids(DirectX::SpriteBatch & batch);
	Asteroid *checkCollAsteroids(Asteroid& me, int& score);

	//Coin Stuff
	void initScore();
	Sprite mCoinSpin;
	DirectX::SpriteFont *mpFont = nullptr;
	
	//once we start thrusting we have to keep doing it for 
	//at least a fraction of a second or it looks whack
	float mThrusting = 0; 

	//setup once
	void InitBgnd();
	void InitPlayer();

	//Menu stuff
	Sprite intro;
	Sprite gameover;


	//make it move, reset it once it leaves the screen, only one at once
	void UpdateMissile(float dTime);
	//make it scroll parallax
	void UpdateBgnd(float dTime);
	//move the ship by keyboard, gamepad or mouse
	void UpdateInput(float dTime);
	//make the flames wobble when the ship moves
	void UpdateThrust(float dTime);
};


/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { PLAY, IntroMode, GOverMode };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::IntroMode;
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime);
	void Render(float dTime);

private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
	//not much of a game, but this is it
	PlayMode mPMode;
};


